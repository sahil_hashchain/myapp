import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Vendor} from '../models';
import {VendorRepository} from '../repositories';

export class VendorController {
  constructor(
    @repository(VendorRepository)
    public vendorRepository : VendorRepository,
  ) {}

  @post('/vendors', {
    responses: {
      '200': {
        description: 'Vendor model instance',
        content: {'application/json': {schema: {'x-ts-type': Vendor}}},
      },
    },
  })
  async create(@requestBody() vendor: Vendor): Promise<Vendor> {
    return await this.vendorRepository.create(vendor);
  }

  @get('/vendors/count', {
    responses: {
      '200': {
        description: 'Vendor model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Vendor)) where?: Where<Vendor>,
  ): Promise<Count> {
    return await this.vendorRepository.count(where);
  }

  @get('/vendors', {
    responses: {
      '200': {
        description: 'Array of Vendor model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Vendor}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Vendor)) filter?: Filter<Vendor>,
  ): Promise<Vendor[]> {
    return await this.vendorRepository.find(filter);
  }

  @patch('/vendors', {
    responses: {
      '200': {
        description: 'Vendor PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Vendor, {partial: true}),
        },
      },
    })
    vendor: Vendor,
    @param.query.object('where', getWhereSchemaFor(Vendor)) where?: Where<Vendor>,
  ): Promise<Count> {
    return await this.vendorRepository.updateAll(vendor, where);
  }

  @get('/vendors/{id}', {
    responses: {
      '200': {
        description: 'Vendor model instance',
        content: {'application/json': {schema: {'x-ts-type': Vendor}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Vendor> {
    return await this.vendorRepository.findById(id);
  }

  @patch('/vendors/{id}', {
    responses: {
      '204': {
        description: 'Vendor PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Vendor, {partial: true}),
        },
      },
    })
    vendor: Vendor,
  ): Promise<void> {
    await this.vendorRepository.updateById(id, vendor);
  }

  @put('/vendors/{id}', {
    responses: {
      '204': {
        description: 'Vendor PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() vendor: Vendor,
  ): Promise<void> {
    await this.vendorRepository.replaceById(id, vendor);
  }

  @del('/vendors/{id}', {
    responses: {
      '204': {
        description: 'Vendor DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.vendorRepository.deleteById(id);
  }
}
