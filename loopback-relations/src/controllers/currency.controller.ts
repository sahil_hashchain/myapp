import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Currency} from '../models';
import {CurrencyRepository} from '../repositories';

export class CurrencyController {
  constructor(
    @repository(CurrencyRepository)
    public currencyRepository : CurrencyRepository,
  ) {}

  @post('/currencies', {
    responses: {
      '200': {
        description: 'Currency model instance',
        content: {'application/json': {schema: {'x-ts-type': Currency}}},
      },
    },
  })
  async create(@requestBody() currency: Currency): Promise<Currency> {
    return await this.currencyRepository.create(currency);
  }

  @get('/currencies/count', {
    responses: {
      '200': {
        description: 'Currency model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Currency)) where?: Where<Currency>,
  ): Promise<Count> {
    return await this.currencyRepository.count(where);
  }

  @get('/currencies', {
    responses: {
      '200': {
        description: 'Array of Currency model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Currency}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Currency)) filter?: Filter<Currency>,
  ): Promise<Currency[]> {
    return await this.currencyRepository.find(filter);
  }

  @patch('/currencies', {
    responses: {
      '200': {
        description: 'Currency PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Currency, {partial: true}),
        },
      },
    })
    currency: Currency,
    @param.query.object('where', getWhereSchemaFor(Currency)) where?: Where<Currency>,
  ): Promise<Count> {
    return await this.currencyRepository.updateAll(currency, where);
  }

  @get('/currencies/{id}', {
    responses: {
      '200': {
        description: 'Currency model instance',
        content: {'application/json': {schema: {'x-ts-type': Currency}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Currency> {
    return await this.currencyRepository.findById(id);
  }

  @patch('/currencies/{id}', {
    responses: {
      '204': {
        description: 'Currency PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Currency, {partial: true}),
        },
      },
    })
    currency: Currency,
  ): Promise<void> {
    await this.currencyRepository.updateById(id, currency);
  }

  @put('/currencies/{id}', {
    responses: {
      '204': {
        description: 'Currency PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() currency: Currency,
  ): Promise<void> {
    await this.currencyRepository.replaceById(id, currency);
  }

  @del('/currencies/{id}', {
    responses: {
      '204': {
        description: 'Currency DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.currencyRepository.deleteById(id);
  }
}
