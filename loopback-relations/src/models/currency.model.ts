import { Entity, model, property, hasMany, hasOne } from '@loopback/repository';
import { Company, CompanyWithRelations } from '.';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'currency' } }
})
export class Currency extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: { "columnName": "currencyid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  currencyid: Number;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "country", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  country?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "currencyName", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  currencyname?: String;

  @hasMany(() => Company, { keyTo: 'basecurrencyid' })
  companies: Company[];


  constructor(data?: Partial<Currency>) {
    super(data);
  }
}

export interface CurrencyRelations {
  companies?: CompanyWithRelations[];
}

export type CurrencyWithRelations = Currency & CurrencyRelations;
