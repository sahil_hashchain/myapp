import { Entity, model, property, hasMany, belongsTo, hasOne } from '@loopback/repository';
import { Customer, CustomerWithRelations } from './customer.model';
import { Contact, ContactWithRelations } from './contact.model';
import { Address } from '.';
import { AddressWithRelations } from './address.model';
import { Currency, CurrencyWithRelations } from './currency.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'company' } }
})
export class Company extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: { "columnName": "id", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  id: Number;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "organization", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  organization?: String;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "structuretypeid", "dataType": "smallint", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  structuretypeid?: Number;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "name", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  name?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "legalname", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  legalname?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "companyemail", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  companyemail?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "website", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  website?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "sector", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  sector?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "registercode", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  registercode?: String;

  @property({
    type: Date,
    required: false,
    postgresql: { "columnName": "registerdate", "dataType": "date", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  registerdate?: Date;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "addressid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  addressid?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "customerfaceaddressid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  customerfaceaddressid?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "contactid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  contactid?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "customerfacecontactid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  customerfacecontactid?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "invoicepaymentterms", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  invoicepaymentterms?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "numberofemployees", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  numberofemployees?: Number;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "companyblockchainid", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  companyblockchainid?: String;

  @property({
    type: Boolean,
    required: false,
    postgresql: { "columnName": "isactive", "dataType": "boolean", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  isactive?: Boolean;

  @property({
    type: Boolean,
    required: false,
    postgresql: { "columnName": "isdeleted", "dataType": "boolean", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  isdeleted?: Boolean;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "legaladdressid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  legaladdressid?: Number;

  // @property({
  //   type: Number,
  //   required: false,
  //   scale: 0,
  //   postgresql: { "columnName": "legalcontactid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  // })
  // legalcontactid?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "basecurrencyid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  basecurrencyid?: Number;

  @property({
    type: Date,
    required: false,
    postgresql: { "columnName": "OnBoardedDate", "dataType": "date", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  onboardeddate?: Date;

  @hasMany(() => Customer)
  customers: Customer[];

  @belongsTo(() => Contact)
  contactId?: Number;

  @belongsTo(() => Contact)
  legalcontactId?: Number;

  @belongsTo(() => Contact)
  customerfacecontactId?: Number;

  @belongsTo(() => Address)
  addressId?: Number;

  @belongsTo(() => Address)
  legaladdressId?: Number;

  @belongsTo(() => Address)
  customerfaceaddressId?: Number;

  @belongsTo(() => Currency, { keyTo: 'basecurrencyid' })
  basecurrencyId?: Number;

  constructor(data?: Partial<Company>) {
    super(data);
  }
}

export interface CompanyRelations {
  customers?: CustomerWithRelations[];
  contact?: ContactWithRelations;
  legalcontact?: ContactWithRelations;
  customerfacecontact?: ContactWithRelations;
  address?: AddressWithRelations;
  legaladdress?: AddressWithRelations;
  customerfaceaddress?: AddressWithRelations;
  basecurrency?: CurrencyWithRelations;
}

export type CompanyWithRelations = Company & CompanyRelations;
