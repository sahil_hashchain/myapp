import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Company, CompanyWithRelations } from './company.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'customer' } }
})
export class Customer extends Entity {
  @property({
    type: Number,
    required: false,
    scale: 0,
    id: true,
    postgresql: { "columnName": "customerid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  customerid: Number;


  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "companyid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  companyid: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "invitationStatusTypeIdl", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  invitationstatustypeidl?: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "referencecompanyid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  referencecompanyid: Number;


  @belongsTo(() => Company)
  companyId?: Number;

  @belongsTo(() => Company)
  referenceCompanyId?: Number;


  constructor(data?: Partial<Customer>) {
    super(data);
  }
}

export interface CustomerRelations {
  company?: CompanyWithRelations
  referenceCompany?: CompanyWithRelations
}

export type CustomerWithRelations = Customer & CustomerRelations;
