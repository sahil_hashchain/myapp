import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Company, CompanyWithRelations } from '.';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'vendor' } }
})
export class Vendor extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: { "columnName": "vendorid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  vendorid: Number;

  @property({
    type: Number,
    required: false,
    scale: 0,
    postgresql: { "columnName": "partytaxschemeid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "YES" },
  })
  partytaxschemeid?: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: { "columnName": "companyid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  companyid: Number;

  @property({
    type: Number,
    required: true,
    scale: 0,
    postgresql: { "columnName": "referencecompanyid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  referencecompanyid: Number;


  @belongsTo(() => Company)
  companyId?: Number;

  @belongsTo(() => Company)
  referenceCompanyId?: Number;

  constructor(data?: Partial<Vendor>) {
    super(data);
  }
}

export interface VendorRelations {
  company?: CompanyWithRelations
  referenceCompany?: CompanyWithRelations
}

export type VendorWithRelations = Vendor & VendorRelations;
