import {Entity, model, property, hasMany} from '@loopback/repository';
import { Company, CompanyWithRelations } from '.';

@model({
  settings: {idInjection: false, postgresql: {schema: 'public', table: 'address'}}
})
export class Address extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: {"columnName":"addressid","dataType":"integer","dataLength":null,"dataPrecision":null,"dataScale":0,"nullable":"NO"},
  })
  addressid: Number;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"streetName","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  streetname?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"buildingName","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  buildingname?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"buildingNumber","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  buildingnumber?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"cityName","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  cityname?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"postalZone","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  postalzone?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"state","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  state?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"addressLine1","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  addressline1?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"addressLine2","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  addressline2?: String;

  @property({
    type: String,
    required: false,
    postgresql: {"columnName":"country","dataType":"character varying","dataLength":null,"dataPrecision":null,"dataScale":null,"nullable":"YES"},
  })
  country?: String;




  @hasMany(() => Company)
  companies: Company[];



  constructor(data?: Partial<Address>) {
    super(data);
  }
}

export interface AddressRelations {
  companies?: CompanyWithRelations[];
}

export type AddressWithRelations = Address & AddressRelations;
