import { Entity, model, property, hasMany } from '@loopback/repository';
import { Company, CompanyWithRelations } from './company.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'contact' } }
})
export class Contact extends Entity {
  @property({
    type: Number,
    required: true,
    scale: 0,
    id: 1,
    postgresql: { "columnName": "contactid", "dataType": "integer", "dataLength": null, "dataPrecision": null, "dataScale": 0, "nullable": "NO" },
  })
  contactid: Number;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "name", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  name?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "telefax", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  telefax?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "email", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  email?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "position", "dataType": "character varying", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  position?: String;

  @property({
    type: String,
    required: false,
    postgresql: { "columnName": "telephone", "dataType": "text", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "YES" },
  })
  telephone?: String;

  @hasMany(() => Company)
  companies: Company[];

  constructor(data?: Partial<Contact>) {
    super(data);
  }
}

export interface ContactRelations {
  companies?: CompanyWithRelations[];
}

export type ContactWithRelations = Contact & ContactRelations;
