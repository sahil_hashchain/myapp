import { DefaultCrudRepository, HasManyRepositoryFactory, repository, Filter, Options } from '@loopback/repository';
import { Address, AddressRelations, Company, AddressWithRelations } from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { CompanyRepository } from '.';

export class AddressRepository extends DefaultCrudRepository<
  Address,
  typeof Address.prototype.addressid,
  AddressRelations
  > {
  public readonly companies: HasManyRepositoryFactory<Company, typeof Address.prototype.addressid>;
  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('CompanyRepository') protected companiesRepositoryGetter: Getter<CompanyRepository>
  ) {
    super(Address, dataSource);
    this.companies = this.createHasManyRepositoryFactoryFor('companies', companiesRepositoryGetter);
  }


  async find(
    filter?: Filter<Address>,
    options?: Options,
  ): Promise<AddressWithRelations[]> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.find(filter, options);

    if (include && include.length && include[0].relation === 'companies') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.companies = await this.companies(r.addressid).find();
        }),
      );
    }

    return result;
  }
  async findById(
    id: typeof Address.prototype.addressid,
    filter?: Filter<Address>,
    options?: Options,
  ): Promise<AddressWithRelations> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.findById(id, filter, options);
    if (include && include.length && include[0].relation === 'companies') {
      result.companies = await this.companies(result.addressid).find();
    }
    return result;
  }
}
