/* eslint-disable require-atomic-updates */
import { DefaultCrudRepository, HasManyRepositoryFactory, repository, Filter, Options, BelongsToAccessor, HasOneRepositoryFactory } from '@loopback/repository';
import { Company, CompanyRelations, CompanyWithRelations } from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { Customer } from '../models/customer.model';
import { CustomerRepository } from './customer.repository';
import { Contact } from '../models/contact.model';
import { ContactRepository } from './contact.repository';
import { Address } from '../models/address.model';
import { AddressRepository } from './address.repository';
import { Currency } from '../models/currency.model';
import { CurrencyRepository } from './currency.repository';

export class CompanyRepository extends DefaultCrudRepository<Company, typeof Company.prototype.id, CompanyRelations>
{
  public readonly customers: HasManyRepositoryFactory<Customer, typeof Company.prototype.id>;

  public readonly contact: BelongsToAccessor<Contact, typeof Company.prototype.id>;
  public readonly legalcontact: BelongsToAccessor<Contact, typeof Company.prototype.id>;
  public readonly customerfacecontact: BelongsToAccessor<Contact, typeof Company.prototype.id>;

  public readonly address: BelongsToAccessor<Address, typeof Company.prototype.id>;
  public readonly legaladdress: BelongsToAccessor<Address, typeof Company.prototype.id>;
  public readonly customerfaceaddress: BelongsToAccessor<Address, typeof Company.prototype.id>;

  public readonly basecurrency: BelongsToAccessor<Currency, typeof Company.prototype.id>;



  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('CustomerRepository') protected customerRepositoryGetter: Getter<CustomerRepository>,

    @repository.getter('ContactRepository') protected contactRepositoryGetter: Getter<ContactRepository>,
    @repository.getter('ContactRepository') protected legalcontactRepositoryGetter: Getter<ContactRepository>,
    @repository.getter('ContactRepository') protected customerfacecontactRepositoryGetter: Getter<ContactRepository>,


    @repository.getter('AddressRepository') protected addressRepositoryGetter: Getter<AddressRepository>,
    @repository.getter('AddressRepository') protected legaladdressRepositoryGetter: Getter<AddressRepository>,
    @repository.getter('AddressRepository') protected customerfaceaddressRepositoryGetter: Getter<AddressRepository>,

    @repository.getter('CurrencyRepository') protected basecurrencyRepositoryGetter: Getter<CurrencyRepository>





  ) {

    super(Company, dataSource);
    this.customers = this.createHasManyRepositoryFactoryFor('customers', customerRepositoryGetter);

    this.legalcontact = this.createBelongsToAccessorFor('legalcontact', legalcontactRepositoryGetter);
    this.contact = this.createBelongsToAccessorFor('contact', contactRepositoryGetter);
    this.customerfacecontact = this.createBelongsToAccessorFor('customerfacecontact', customerfacecontactRepositoryGetter);

    this.address = this.createBelongsToAccessorFor('address', addressRepositoryGetter);
    this.legaladdress = this.createBelongsToAccessorFor('legaladdress', addressRepositoryGetter);
    this.customerfaceaddress = this.createBelongsToAccessorFor('customerfaceaddress', addressRepositoryGetter);
    this.basecurrency = this.createBelongsToAccessorFor('basecurrency', basecurrencyRepositoryGetter);



  }

  async find(
    filter?: Filter<Company>,
    options?: Options,
  ): Promise<CompanyWithRelations[]> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.find(filter, options);

    if (include && include.length && include[0].relation === 'customers') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.customers = await this.customers(r.id).find();
        }),
      );
    }

    if (include && include.length && include[0].relation === 'all') {
      await Promise.all(
        result.map(async r => {
          r.customers = await this.customers(r.id).find();
          r.contact = await this.contact(r.id);
          r.legalcontact = await this.legalcontact(r.id);
          r.customerfacecontact = await this.customerfacecontact(r.id);
          r.address = await this.address(r.id);
          r.legaladdress = await this.legaladdress(r.id);
          r.customerfaceaddress = await this.customerfaceaddress(r.id);
          r.basecurrency = await this.basecurrency(r.id);
        }),
      );
    }

    return result;
  }
  async findById(
    id: typeof Company.prototype.id,
    filter?: Filter<Company>,
    options?: Options,
  ): Promise<CompanyWithRelations> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.findById(id, filter, options);
    if (include && include.length && include[0].relation === 'customers') {
      result.customers = await this.customers(result.id).find();
    }

    if (include && include.length && include[0].relation === 'all') {
      result.customers = await this.customers(result.id).find();
      result.contact = await this.contact(result.id);
      result.legalcontact = await this.legalcontact(result.id);
      result.customerfacecontact = await this.customerfacecontact(result.id);
      result.address = await this.address(result.id);
      result.legaladdress = await this.legaladdress(result.id);
      result.customerfaceaddress = await this.customerfaceaddress(result.id);
      result.basecurrency = await this.basecurrency(result.id);
    }

    return result;
  }






}
