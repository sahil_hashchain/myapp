import { DefaultCrudRepository, HasManyRepositoryFactory, repository, Filter, Options } from '@loopback/repository';
import { Contact, ContactRelations, Company } from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { CompanyRepository } from '.';
import { ContactWithRelations } from '../models/contact.model';

export class ContactRepository extends DefaultCrudRepository<Contact, typeof Contact.prototype.contactid, ContactRelations> {


  public readonly companies: HasManyRepositoryFactory<Company, typeof Contact.prototype.contactid>;
  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('CompanyRepository') protected companiesRepositoryGetter: Getter<CompanyRepository>
  ) {
    super(Contact, dataSource);
    this.companies = this.createHasManyRepositoryFactoryFor('companies', companiesRepositoryGetter);
  }


  async find(
    filter?: Filter<Contact>,
    options?: Options,
  ): Promise<ContactWithRelations[]> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.find(filter, options);

    if (include && include.length && include[0].relation === 'companies') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.companies = await this.companies(r.contactid).find();
        }),
      );
    }

    return result;
  }
  async findById(
    id: typeof Contact.prototype.contactid,
    filter?: Filter<Contact>,
    options?: Options,
  ): Promise<ContactWithRelations> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.findById(id, filter, options);
    if (include && include.length && include[0].relation === 'companies') {
      result.companies = await this.companies(result.contactid).find();
    }
    return result;
  }








}
