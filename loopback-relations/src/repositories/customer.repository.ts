import { DefaultCrudRepository, BelongsToAccessor, repository, Filter, Options } from '@loopback/repository';
import { Customer, CustomerRelations, Company, CustomerWithRelations } from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { CompanyRepository } from '.';

export class CustomerRepository extends DefaultCrudRepository<Customer, typeof Customer.prototype.customerid, CustomerRelations> {

  public readonly company: BelongsToAccessor<Company, typeof Customer.prototype.customerid>;
  public readonly referencecompany: BelongsToAccessor<Company, typeof Customer.prototype.customerid>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,

    @repository.getter('CompanyRepository')
    protected companyRepositoryGetter: Getter<CompanyRepository>,

    @repository.getter('CompanyRepository')
    protected companyreferenceRepositoryGetter: Getter<CompanyRepository>,

  ) {
    super(Customer, dataSource);
    this.referencecompany = this.createBelongsToAccessorFor('referenceCompany', companyreferenceRepositoryGetter);
    this.company = this.createBelongsToAccessorFor('company', companyRepositoryGetter);
  }

  async find(
    filter?: Filter<Customer>,
    options?: Options,
  ): Promise<CustomerWithRelations[]> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.find(filter, options);
    if (include && include.length && include[0].relation === 'company') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.company = await this.company(r.customerid);
        }),
      );
    }

    if (include && include.length && include[0].relation === 'all') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.company = await this.company(r.customerid);
          // eslint-disable-next-line require-atomic-updates
          r.referenceCompany = await this.referencecompany(r.customerid)
        }),
      );
    }

    return result;
  }

  async findById(
    id: typeof Customer.prototype.customerid,
    filter?: Filter<Customer>,
    options?: Options,
  ): Promise<CustomerWithRelations> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.findById(id, filter, options);
    if (include && include.length && include[0].relation === 'company') {
      result.company = await this.company(result.customerid);
      result.referenceCompany = await this.company(result.customerid)
    }

    if (include && include.length && include[0].relation === 'all') {
      // eslint-disable-next-line require-atomic-updates
      result.company = await this.company(result.customerid);
      // eslint-disable-next-line require-atomic-updates
      result.referenceCompany = await this.referencecompany(result.customerid)


    }
    return result;
  }

}
