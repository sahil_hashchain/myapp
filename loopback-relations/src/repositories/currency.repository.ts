import { DefaultCrudRepository, HasOneRepositoryFactory, repository, Filter, Options, hasMany, HasManyRepositoryFactory } from '@loopback/repository';
import { Currency, CurrencyRelations, Company, CurrencyWithRelations } from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { CompanyRepository } from './company.repository';

export class CurrencyRepository extends DefaultCrudRepository<Currency, typeof Currency.prototype.currencyid, CurrencyRelations> {

  public readonly companies: HasManyRepositoryFactory<Company, typeof Currency.prototype.currencyid>;



  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('CompanyRepository')
    protected companyRepositoryGetter: Getter<CompanyRepository>,
  ) {
    super(Currency, dataSource);
    this.companies = this.createHasManyRepositoryFactoryFor('companies', companyRepositoryGetter)
  }

  async find(
    filter?: Filter<Currency>,
    options?: Options,
  ): Promise<CurrencyWithRelations[]> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.find(filter, options);

    if (include && include.length && include[0].relation === 'companies') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.companies = await this.companies(r.currencyid).find();
        }),
      );
    }

    return result;
  }
  async findById(
    id: typeof Currency.prototype.currencyid,
    filter?: Filter<Currency>,
    options?: Options,
  ): Promise<CurrencyWithRelations> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.findById(id, filter, options);
    if (include && include.length && include[0].relation === 'companies') {
      result.companies = await this.companies(result.currencyid).find();
    }
    return result;
  }





}
