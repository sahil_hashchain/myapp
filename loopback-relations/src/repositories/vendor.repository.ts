import {DefaultCrudRepository, BelongsToAccessor, repository, Filter, Options} from '@loopback/repository';
import {Vendor, VendorRelations, Company, VendorWithRelations} from '../models';
import {PostgresDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import { CompanyRepository } from '.';

export class VendorRepository extends DefaultCrudRepository<
  Vendor,
  typeof Vendor.prototype.vendorid,
  VendorRelations
> {

  public readonly company: BelongsToAccessor<Company, typeof Vendor.prototype.vendorid>;
  public readonly referencecompany: BelongsToAccessor<Company, typeof Vendor.prototype.vendorid>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('CompanyRepository')
    protected companyRepositoryGetter: Getter<CompanyRepository>,

    @repository.getter('CompanyRepository')
    protected companyreferenceRepositoryGetter: Getter<CompanyRepository>,
  ) {
    super(Vendor, dataSource);
    this.referencecompany = this.createBelongsToAccessorFor('referenceCompany', companyreferenceRepositoryGetter);
    this.company = this.createBelongsToAccessorFor('company', companyRepositoryGetter);
  }

  async find(
    filter?: Filter<Vendor>,
    options?: Options,
  ): Promise<VendorWithRelations[]> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.find(filter, options);
    if (include && include.length && include[0].relation === 'company') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.company = await this.company(r.vendorid);
        }),
      );
    }

    if (include && include.length && include[0].relation === 'all') {
      await Promise.all(
        result.map(async r => {
          // eslint-disable-next-line require-atomic-updates
          r.company = await this.company(r.vendorid);
          // eslint-disable-next-line require-atomic-updates
          r.referenceCompany = await this.referencecompany(r.vendorid)
        }),
      );
    }

    return result;
  }

  async findById(
    id: typeof Vendor.prototype.vendorid,
    filter?: Filter<Vendor>,
    options?: Options,
  ): Promise<VendorWithRelations> {
    const include = filter && filter.include;
    filter = { ...filter, include: undefined };
    const result = await super.findById(id, filter, options);
    if (include && include.length && include[0].relation === 'company') {
      result.company = await this.company(result.vendorid);
      result.referenceCompany = await this.company(result.vendorid)
    }

    if (include && include.length && include[0].relation === 'all') {
      // eslint-disable-next-line require-atomic-updates
      result.company = await this.company(result.vendorid);
      // eslint-disable-next-line require-atomic-updates
      result.referenceCompany = await this.referencecompany(result.vendorid)


    }
    return result;
  }
}
